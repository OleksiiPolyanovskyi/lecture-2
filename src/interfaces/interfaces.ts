export interface Controls {
    PlayerOneAttack:string;
    PlayerOneBlock:string;
    PlayerTwoAttack:string;
    PlayerTwoBlock:string;
    PlayerOneCritical:string[];
    PlayerTwoCritical:string[]
};

export interface Fighter {
    _id:string;
    name:string;
    source:string;
};

export interface FighterDetails extends Fighter {    
    health:number;
    attack:number;
    defense:number;
};

export interface FighterPreview {
    name:string;
    health:number;
    attack:number;
    defense:number;
}

export interface FighterState {
    fighterInDefence:boolean;
    fighterHealth: number;
    fighterCritical: Set<string>;
    fighterCriticalAllowed: boolean;
    fighterIndicatorWidth:number  
}
    
export interface FightState {
    first:FighterState;
    second:FighterState
}
    
export interface ChangeStateInput {
    state:FightState;
    fighterAttacker:FighterDetails;
    fighterDefender:FighterDetails;
    indicator:HTMLElement;  
    attacker:keyof FightState;
    defender:keyof FightState
}

interface ModalArguments {
    title:string;
    bodyElement:HTMLElement;
    onClose?:()=>void
}

export interface ShowModal {
    (a:ModalArguments):void
}   
    
export interface CreateModal {
    (a:ModalArguments):HTMLElement
} 

