import { Fighter, FighterDetails } from '../interfaces/interfaces';

export type ApiResponse = Fighter[] | FighterDetails;

export type SelectedFighters = {
    leftFighter:FighterDetails,
    rightFighter:FighterDetails
}
    