import { Controls } from '../interfaces/interfaces'

export const controls:Controls = {
  PlayerOneAttack: 'KeyA',
  PlayerOneBlock: 'KeyD',
  PlayerTwoAttack: 'KeyJ',
  PlayerTwoBlock: 'KeyL',
  PlayerOneCritical: ['KeyQ', 'KeyW', 'KeyE'],
  PlayerTwoCritical: ['KeyU', 'KeyI', 'KeyO']
}