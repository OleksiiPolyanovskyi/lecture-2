import { createElement } from '../helpers/domHelper';
import { renderArena } from './arena';
const versusImg = require('../../../resources/versus.png');
import { createFighterPreview } from './fighterPreview';
import { fighterService } from '../services/fightersService';
import { FighterDetails } from '../../interfaces/interfaces';
import { SelectedFighters } from '../../types/types';

export function createFightersSelector():(a:Event, b:string)=>void {
  let selectedFighters:FighterDetails[] = [];

  return async (event:Event, fighterId:string) => {
    const fighter = await getFighterInfo(fighterId);    
    const [playerOne, playerTwo] = selectedFighters;
    const firstFighter = playerOne ?? fighter;
    const secondFighter = Boolean(playerOne) ? playerTwo ?? fighter : playerTwo;
    selectedFighters = [firstFighter, secondFighter];

    renderSelectedFighters(selectedFighters);
  };
}

const fighterDetailsMap = new Map();

export async function getFighterInfo(fighterId:string) {
  // get fighter info from fighterDetailsMap or from service and write it to fighterDetailsMap  
  const fighterDetails = await fighterService.getFighterDetails(fighterId);   
  return fighterDetails;
}

function renderSelectedFighters(selectedFighters:FighterDetails[]):void {
  const fightersPreview:Element = document.querySelector('.preview-container___root')!;
  const [playerOne, playerTwo] = selectedFighters;
  const firstPreview:HTMLElement = createFighterPreview(playerOne, 'left');
  const secondPreview:HTMLElement = createFighterPreview(playerTwo, 'right');
  const versusBlock:HTMLElement = createVersusBlock(selectedFighters);

  fightersPreview.innerHTML = '';
  fightersPreview.append(firstPreview, versusBlock, secondPreview);
}

function createVersusBlock(selectedFighters:FighterDetails[]) {
  const canStartFight:boolean = selectedFighters.filter(Boolean).length === 2;
  const onClick = () => startFight(selectedFighters);
  const container:HTMLElement = createElement({ tagName: 'div', className: 'preview-container___versus-block' });
  const image:HTMLElement = createElement({
    tagName: 'img',
    className: 'preview-container___versus-img',
    attributes: { src: versusImg },
  });
  const disabledBtn = canStartFight ? '' : 'disabled';
  const fightBtn:HTMLElement = createElement({
    tagName: 'button',
    className: `preview-container___fight-btn`,
  });

  fightBtn.addEventListener('click', onClick, false);
  fightBtn.innerText = 'Fight';
  container.append(image, fightBtn);

  return container;
}

function startFight(selectedFighters:FighterDetails[]):void {
  const fighters:SelectedFighters = {
    leftFighter:selectedFighters[0],
    rightFighter:selectedFighters[1]
  }
  renderArena(fighters);
}
