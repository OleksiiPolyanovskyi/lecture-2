import { createElement } from '../helpers/domHelper';
import { FighterDetails, FighterPreview } from '../../interfaces/interfaces';

export function createFighterPreview(fighter:FighterDetails, position:string) {  
    const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
    const fighterElement:HTMLElement = createElement({
      tagName: 'div',
      className: `fighter-preview___root ${positionClassName}`,
    });  
    // todo: show fighter info (image, name, health, etc.) 
    if(fighter && fighter.name){
      const {_id ,source, ...otherProps } = fighter;  
      const other:FighterPreview = {...otherProps};  
      const prewImg = createFighterImage(fighter);
      fighterElement.appendChild(prewImg); 
      const keys = (Object.keys(other) as Array<keyof FighterPreview>);
      keys.forEach(prop => {
        let field = other[prop];
        fighterElement.insertAdjacentHTML('beforeend', `<div class="fighter-preview___text">${prop}: ${field}</div>`) 
      });       
    }; 
    return fighterElement;  
  }
  

export function createFighterImage(fighter:FighterDetails) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement:HTMLElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

