import { createElement } from '../helpers/domHelper';
import { createFighterImage } from './fighterPreview';
import { fight } from './fight';
import { showWinnerModal } from './modal/winner';
import { FighterDetails } from '../../interfaces/interfaces';
import { SelectedFighters } from '../../types/types';

export async function renderArena(selectedFighters:SelectedFighters) {
    const root:HTMLElement = document.getElementById('root')!;
    const arena:HTMLElement = createArena(selectedFighters);
  
    root.innerHTML = '';
    root.append(arena);
    const {leftFighter, rightFighter} = selectedFighters;
    // todo:
    // - start the fight 
    // - when fight is finished show winner
    const winner = await fight(leftFighter, rightFighter);
    showWinnerModal(winner);
  }
  
  function createArena(selectedFighters:SelectedFighters) {
    const arena:HTMLElement = createElement({ tagName: 'div', className: 'arena___root' });
    const {leftFighter, rightFighter} = selectedFighters;
    const healthIndicators:HTMLElement = createHealthIndicators(leftFighter, rightFighter);
    const fighters:HTMLElement = createFighters(leftFighter, rightFighter);
    
    arena.append(healthIndicators, fighters);
    return arena;
  }
  
  function createHealthIndicators(leftFighter:FighterDetails, rightFighter:FighterDetails) {
    const healthIndicators:HTMLElement = createElement({ tagName: 'div', className: 'arena___fight-status' });
    const versusSign:HTMLElement = createElement({ tagName: 'div', className: 'arena___versus-sign' });
    const leftFighterIndicator:HTMLElement = createHealthIndicator(leftFighter, 'left');
    const rightFighterIndicator:HTMLElement = createHealthIndicator(rightFighter, 'right');
  
    healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
    return healthIndicators;
  }
  
  function createHealthIndicator(fighter:FighterDetails, position:string) {
    const { name } = fighter;
    const container:HTMLElement = createElement({ tagName: 'div', className: 'arena___fighter-indicator' });
    const fighterName:HTMLElement = createElement({ tagName: 'span', className: 'arena___fighter-name' });
    const indicator:HTMLElement = createElement({ tagName: 'div', className: 'arena___health-indicator' });
    const bar:HTMLElement = createElement({ tagName: 'div', className: 'arena___health-bar', attributes: { id: `${position}-fighter-indicator` }});
  
    fighterName.innerText = name;
    indicator.append(bar);
    container.append(fighterName, indicator);
  
    return container;
  }
  
  function createFighters(firstFighter:FighterDetails, secondFighter:FighterDetails) {
    const battleField:HTMLElement = createElement({ tagName: 'div', className: `arena___battlefield` });
    const firstFighterElement:HTMLElement = createFighter(firstFighter, 'left');
    const secondFighterElement:HTMLElement = createFighter(secondFighter, 'right');
  
    battleField.append(firstFighterElement, secondFighterElement);
    return battleField;
  }
  
  function createFighter(fighter:FighterDetails, position:string) {
    const imgElement:HTMLElement = createFighterImage(fighter);
    const positionClassName = position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter';
    const fighterElement:HTMLElement = createElement({
      tagName: 'div',
      className: `arena___fighter ${positionClassName}`,
    });
  
    fighterElement.append(imgElement);
    return fighterElement;
  }    
