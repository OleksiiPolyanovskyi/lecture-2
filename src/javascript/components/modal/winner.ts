import showModal from './modal';
import { createFighterImage } from '../fighterPreview';
import { FighterDetails } from '../../../interfaces/interfaces';

export function showWinnerModal(fighter:FighterDetails) {
  // call showModal function 
  const fighterImage = createFighterImage(fighter)
  showModal({title:`Winner: ${fighter.name}`, bodyElement: fighterImage})
}
