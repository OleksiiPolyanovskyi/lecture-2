import { controls } from '../../constants/controls';
import { FightState, FighterDetails, ChangeStateInput } from '../../interfaces/interfaces';

export async function fight(firstFighter:FighterDetails, secondFighter:FighterDetails):Promise<FighterDetails> {

    let leftFighterIndicator:HTMLElement = document.getElementById('left-fighter-indicator')!;
    let rightFighterIndicator:HTMLElement = document.getElementById('right-fighter-indicator')!;
  
    const state:FightState = {
      first: {
        fighterInDefence: false,
        fighterHealth: firstFighter.health,
        fighterCritical: new Set(),
        fighterCriticalAllowed: true,
        fighterIndicatorWidth: 100      
      },
      second: {
        fighterInDefence: false,
        fighterHealth: secondFighter.health,
        fighterCritical: new Set(),
        fighterCriticalAllowed: true,
        fighterIndicatorWidth: 100      
      }    
    };  
  
    return new Promise((resolve) => {
      // resolve the promise with the winner when fight is over    
      document.addEventListener('keydown', event => {
       
        switch(event.code){
          case controls.PlayerOneAttack:                  
            changeStateForHit(
             {
               state, 
               fighterAttacker:firstFighter, 
               fighterDefender: secondFighter, 
               indicator:rightFighterIndicator, 
               attacker:'first', 
               defender:'second'
              });     
            if (state.second.fighterHealth < 0) resolve(firstFighter);
            break;
          case controls.PlayerTwoAttack:
            changeStateForHit(
              {
                state,
                fighterAttacker:secondFighter, 
                fighterDefender: firstFighter,               
                indicator: leftFighterIndicator, 
                attacker:'second', 
                defender:'first'
              });      
            if (state.first.fighterHealth < 0) resolve(secondFighter);
            break;
          case controls.PlayerOneBlock:
            state.first.fighterInDefence = true;             
            break;
          case controls.PlayerTwoBlock:
            state.second.fighterInDefence = true;         
            break;
          case controls.PlayerOneCritical[0]:
            state.first.fighterCritical.add(controls.PlayerOneCritical[0]);         
            if (state.first.fighterCriticalAllowed){
              changeStateForCriticalHit(
                {
                  state,
                  fighterAttacker: firstFighter, 
                  fighterDefender: secondFighter, 
                  indicator: rightFighterIndicator,
                  attacker:'first', 
                  defender:'second'                
                })
            }  
            if (state.second.fighterHealth < 0) resolve(firstFighter);
            break;
          case controls.PlayerOneCritical[1]:
            state.first.fighterCritical.add(controls.PlayerOneCritical[1]);          
            if (state.first.fighterCriticalAllowed){
              changeStateForCriticalHit(
                {
                  state,
                  fighterAttacker: firstFighter, 
                  fighterDefender: secondFighter, 
                  indicator: leftFighterIndicator,
                  attacker:'first', 
                  defender:'second'                
                })
            }  
            if (state.second.fighterHealth < 0) resolve(firstFighter);
            break;
          case controls.PlayerOneCritical[2]:
            state.first.fighterCritical.add(controls.PlayerOneCritical[2]);
            if (state.first.fighterCriticalAllowed){
              changeStateForCriticalHit(
                {
                  state,
                  fighterAttacker: firstFighter, 
                  fighterDefender: secondFighter, 
                  indicator: rightFighterIndicator,
                  attacker:'first', 
                  defender:'second'                
                })
            }              
            if (state.second.fighterHealth < 0) resolve(firstFighter);
            break;
          case controls.PlayerTwoCritical[0]:
            state.second.fighterCritical.add(controls.PlayerTwoCritical[0]);
            if (state.second.fighterCriticalAllowed){
              changeStateForCriticalHit(
                {
                  state,
                  fighterAttacker: secondFighter, 
                  fighterDefender: firstFighter, 
                  indicator: leftFighterIndicator,
                  attacker:'second', 
                  defender:'first'                
                })
            } 
            if (state.first.fighterHealth < 0) resolve(secondFighter);  
            break;
          case controls.PlayerTwoCritical[1]:
            state.second.fighterCritical.add(controls.PlayerTwoCritical[1]);
            if (state.second.fighterCriticalAllowed){
              changeStateForCriticalHit(
                {
                  state,
                  fighterAttacker: secondFighter, 
                  fighterDefender: firstFighter, 
                  indicator: leftFighterIndicator,
                  attacker:'second', 
                  defender:'first'                
                })
            } 
            if (state.first.fighterHealth < 0) resolve(secondFighter);   
            break;
          case controls.PlayerTwoCritical[2]:
            state.second.fighterCritical.add(controls.PlayerTwoCritical[2]);
            if (state.second.fighterCriticalAllowed){
              changeStateForCriticalHit(
                {
                  state,
                  fighterAttacker: secondFighter, 
                  fighterDefender: firstFighter, 
                  indicator: leftFighterIndicator, 
                  attacker:'second', 
                  defender:'first'                
                })
            } 
            if (state.first.fighterHealth < 0) resolve(secondFighter);   
            break;
        }
      })
      document.addEventListener('keyup', event => {       
        switch(event.code){        
          case controls.PlayerOneBlock:
            state.first.fighterInDefence = false;         
            break;
          case controls.PlayerTwoBlock:
            state.second.fighterInDefence = false;         
            break;
          case controls.PlayerOneCritical[0]:
            state.first.fighterCritical.delete(controls.PlayerOneCritical[0]);          
            break;
          case controls.PlayerOneCritical[1]:
            state.first.fighterCritical.delete(controls.PlayerOneCritical[1]);         
            break;
          case controls.PlayerOneCritical[2]:
            state.first.fighterCritical.delete(controls.PlayerOneCritical[2]);          
            break;
          case controls.PlayerTwoCritical[0]:
            state.second.fighterCritical.delete(controls.PlayerTwoCritical[0]);            
            break;
          case controls.PlayerTwoCritical[1]:
            state.second.fighterCritical.delete(controls.PlayerTwoCritical[1]);            
            break;
          case controls.PlayerTwoCritical[2]:
             state.second.fighterCritical.delete(controls.PlayerTwoCritical[2]);            
            break;
        }
      })
    });
  }
  
  export function getDamage(attacker:FighterDetails | null, defender:FighterDetails, fighterInDefence:boolean) {
    // return damage  
    const hitPower = getHitPower(attacker);
    const blockPower = getBlockPower(defender);  
    if (hitPower > blockPower && !fighterInDefence){
      return hitPower - blockPower;
    }else{
      return 0;
    }
  }
  
  export function getHitPower(fighter:FighterDetails | null) {
    // return hit power
    if (fighter){
      const criticalHitChance = Math.random() + 1;   
      const power = fighter.attack * criticalHitChance;
      return power;
    }else {
      return 0
    }
  }
  
  export function getBlockPower(fighter:FighterDetails) {
    // return block power 
    const dodgeChance = Math.random() + 1;
    const power = fighter.defense * dodgeChance;
    return power;  
  } 
  
  function changeStateForHit(
    {
      state, 
      fighterAttacker,
      fighterDefender,
      indicator, 
      attacker, 
      defender
    }:ChangeStateInput)
      {
      const damage = getDamage(state[attacker].fighterInDefence? null : fighterAttacker, fighterDefender, state[defender].fighterInDefence);
      state[defender].fighterHealth = state[defender].fighterHealth - damage;
            
      if (damage > 0) {
          state[defender].fighterIndicatorWidth = state[defender].fighterIndicatorWidth - (damage * 100)/fighterDefender.health;
          indicator.style.width =  `${state[defender].fighterIndicatorWidth}%`;
      }  
  }
  
   function changeStateForCriticalHit(
     {
     state,
     fighterAttacker,
     fighterDefender,  
     indicator,  
     attacker, 
     defender   
    }:ChangeStateInput) {
      let damage = getCriticalDamage(fighterAttacker, state[attacker].fighterCritical)
      if (damage > 0) {      
        state[defender].fighterHealth = state[defender].fighterHealth - damage;
        state[attacker].fighterCriticalAllowed = false;
        state[defender].fighterIndicatorWidth = state[defender].fighterIndicatorWidth - (damage * 100)/fighterDefender.health
        indicator.style.width =  `${state[defender].fighterIndicatorWidth}%`
        setTimeout(()=>{
          state[attacker].fighterCriticalAllowed = true;          
        },10000)
      }             
    }
  
    function getCriticalDamage(fighter:FighterDetails, combination:Set<string>){
      if (combination.size === 3) {
        return fighter.attack * 2;
      }else {
        return 0;
      }  
    }
  