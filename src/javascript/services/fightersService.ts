import { callApi } from '../helpers/apiHelper';
import { Fighter, FighterDetails } from '../../interfaces/interfaces';

class FighterService {
    async getFighters(): Promise<Fighter[]>{
      try {
        const endpoint:string = 'fighters.json';
        const apiResult:Fighter[] = await callApi(endpoint, 'GET');
  
        return apiResult;
      } catch (error) {
        throw error;
      }
    }
  
    async getFighterDetails(id:string): Promise<FighterDetails> {
      // todo: implement this method
      // endpoint - `details/fighter/${id}.json`;
      try{
        const endpoint:string = `details/fighter/${id}.json`
        const apiResult: FighterDetails = await callApi(endpoint, 'GET');
  
        return apiResult;
      }catch(error) {
        throw error;
      }
    }
  }
  

export const fighterService = new FighterService();
