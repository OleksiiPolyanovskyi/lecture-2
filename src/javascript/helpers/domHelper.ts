export function createElement
  <T extends { [key: string]: any }, R extends keyof T>
  ({ tagName, className, attributes }:
   { tagName:string; className?:string; attributes?:T}) {
    const element:HTMLElement = document.createElement(tagName);  
    if (className) {
      const classNames = className.split(' ').filter(Boolean);
      element.classList.add(...classNames);
    }  
    
    if (attributes) {
      const keys = (Object.keys(attributes) as Array<R>)
      keys.forEach((key) => element.setAttribute(`${key}`, attributes[key]));
    } 
  
    return element;
  }
  
